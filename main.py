# -*- coding: utf-8  -*-
import re
import pywikibot
import head
import json
import requests

from pywikibot import pagegenerators
from pywikibot.data.sparql import SparqlQuery
from urllib.parse import unquote

def persons(query):
    workpage1 = re.sub(r"#%s", head.allquery[query]["title"], head.side)
    workpage = re.sub(r"#%p", head.allquery[query]["path"], workpage1)
    url1 = re.sub(r"#%s", head.allquery[query]["url"], head.queryTemp)
    url = re.sub(r"#%l", head.allquery[query]["limit"], url1)
    path = head.allquery[query]["path"]

    page = pywikibot.Page(site, path)
    xml = wikiquery.query(url)
    split = page.text.splitlines()
    try:
        if split[-4] == xml["results"]["bindings"][-1]["string"]["value"]: # No changes!
            return ""
    except:
        pywikibot.output(u'Error: Page does not exist! .. Creates page.')

    for information in xml["results"]["bindings"]:
        try:
            workpage += information["string"]["value"] + "\n"
        except:
            continue

    workpage += head.bottom

    page.text = page.text.replace(page.text, workpage)
    page.save(summery)

def check(item):
    if "metadata" in item:
        return "Fil:" + item['metadata']['image'] + "|[[w:se:" + item["title"] + "|" + item["title"].replace('_', ' ') + "]]\n"
    else:
        return "Fil:Image of none.svg|[[w:se:" + item["title"] + "|" + item["title"].replace('_', ' ') + "]]\n"

def places(query):

    newval = ""
    workpage3 = re.sub(r"display:flex", "display:center", head.side)
    workpage2 = re.sub(r"#%s", head.allquery[query]["title"], workpage3)
    workpage1 = re.sub(r"#%p", head.allquery[query]["path"], workpage2)
    workpage = re.sub(r"""<gallery mode="packed" heights="200px" style="color:#000">""", head.allquery[query]["mapframe"], workpage1)
    url = re.sub(r"#%s", head.allquery[query]["url"], head.queryMap)
    path = head.allquery[query]["path"]

    page = pywikibot.Page(site, path)
    xml = wikiquery.query(url)
    try:
        newval = re.findall(r'\"description\":."(.+?)"', page.text[-300:])
        if newval[-1] == xml["results"]["bindings"][-1]["file"]["value"]: # No changes!
            return ""
    except:
        pywikibot.output(u'Error: Page does not exist! .. Creates page.')

    maping = {"type": "FeatureCollection", "features": []}

    for pos in xml["results"]["bindings"]:
        startmap = { "type": "Feature", "geometry": { "type": "Point", "coordinates": [] }, "properties": {"title": "", "description": ""}}
        startmap["geometry"]["coordinates"].append(float(re.sub(r'Point\((\d+.\d+) (\d+.\d+)\)',r'\1',pos["geo"]["value"])))
        startmap["geometry"]["coordinates"].append(float(re.sub(r'Point\((\d+.\d+) (\d+.\d+)\)',r'\2',pos["geo"]["value"])))
        startmap["properties"]["title"] = pos["title"]["value"]
        startmap["properties"]["description"] = pos["file"]["value"]
        maping["features"].append(startmap)

    workpage += json.dumps(maping) + head.bottom2

    page.text = page.text.replace(page.text, workpage)
    page.save(summery)

def stubb(query):
    page = head.side2.replace("#%p", head.allquery[query]["path"])
    page1 = page.replace("#%s", head.allquery[query]["title"])

    pageX = pywikibot.Page(site, head.allquery[query]["path"])

    json_url = requests.get(re.sub(r'%KAT%', head.allquery[query]["url"], head.queryPetscan))
    data = json_url.json()

    smallgallery = ''
    gallery = ''
    for item in data["*"][0]["a"]["*"]:
        if item["len"] >= 1000:
            gallery += check(item)
        else:
            smallgallery += check(item)

    page2 = page1.replace("#%q1",smallgallery)
    page3 = page2.replace("#%q2",gallery)

    try:
        lastPerson = re.findall(r"\[\[w:se:(\S+)\|", page3[-300:])
        lastPersonText = re.findall(r"\[\[w:se:(\S+)\|", pageX.text[-300:])
        if lastPerson[-1] == lastPersonText[-1]:
            return ""
    except:
        pywikibot.output(u'Error: Page does not exist! .. Creates page.')

    pageX.text = pageX.text.replace(pageX.text, page3)
    pageX.save(summery)

summery = "Generert med Wikidata Query"

site = pywikibot.Site('wikimedia', 'wikimedia')  # The site we want to run our bot on

wikiquery = SparqlQuery()

for query in head.allquery:
    if re.search(r'(querySteder)',query):
        places(query)
    elif re.search(r'(queryStubb)',query):
        stubb(query)
    elif re.search(r'(queryPerson)',query):
        persons(query)
