# tool-saami

Tool for updating map coordinates, stubs and red link lists on [Prosjekt:Samisk kunnskap på nett](https://no.wikimedia.org/wiki/Prosjekt:Samisk_kunnskap_p%C3%A5_nett/Undervisning). Uses [https://query.wikidata.org/](https://query.wikidata.org/) and [https://petscan.wmflabs.org/](https://petscan.wmflabs.org/) for data collection.

# Files

Files in the project.

## main.py

The file that is called for the bot to run. Divided into multiple function for different tasks.

## header.py

All of the wiki text that are used on the are copied to the wiki site.
