# -*- coding: utf-8  -*-
queryMap = """SELECT DISTINCT ?id (SAMPLE(?geo) as ?geo) (CONCAT(SAMPLE(?fileName),"{{!}}200px]]") as ?file)  (CONCAT("[[w:se:",?idLabel,"{{!}}<span style=color:red>", ?idLabel, "</span>]]")as ?title) WHERE {
  #%s
        wdt:P625 ?geo;
        wdt:P18 ?image;
        wikibase:sitelinks ?linkcount . hint:Prior hint:rangeSafe true .
  MINUS {
    ?wfr schema:about ?id . ?wfr schema:isPartOf <https://se.wikipedia.org/>.
  }  # No article at sewiki
  BIND(REPLACE(wikibase:decodeUri(STR(?image)), "http://commons.wikimedia.org/wiki/Special:FilePath/", "[[Fil:") as ?fileName) .
  SERVICE wikibase:label { bd:serviceParam wikibase:language "se,nb,da,en".
                         ?id rdfs:label ?idLabel.
                         }
        }
GROUP BY ?id ?idLabel
ORDER BY DESC(MAX(?linkcount))
LIMIT 100"""

queryTemp = """SELECT (CONCAT(SAMPLE(?fileName), "|[[w:se:", ?itemLabel, "|<span style=color:red>", STR(?itemLabel), "</span>]]") as ?string)  WHERE {
     #%s
     wikibase:sitelinks ?linkcount . hint:Prior hint:rangeSafe true .
  ?item wdt:P18 ?image.
  SERVICE wikibase:label { bd:serviceParam wikibase:language "se,nb,nn,da,sv,fi,en".
                         ?item rdfs:label ?itemLabel.}
  BIND(REPLACE(wikibase:decodeUri(STR(?image)), "http://commons.wikimedia.org/wiki/Special:FilePath/", "Fil:") as ?fileName) .
  FILTER (?linkcount > 50)
  MINUS {
   ?wse schema:about ?item .
   ?wse schema:isPartOf <https://se.wikipedia.org/>.
   } # No article at sewiki
}
GROUP BY ?itemLabel
ORDER BY DESC(MAX(?linkcount))
LIMIT #%l"""

queryPetscan = "https://petscan.wmflabs.org/?categories=%KAT%&search_wiki=&cb_labels_any_l=1&max_sitelink_count=&before=&ores_prob_to=&templates_no=&max_age=&search_query=&labels_yes=&cb_labels_yes_l=1&referrer_url=&depth=0&sparql=&manual_list=&edits%5Banons%5D=both&minlinks=&sitelinks_any=&outlinks_any=&outlinks_no=&search_max_results=500&ns%5B0%5D=1&wikidata_source_sites=&labels_any=&cb_labels_no_l=1&edits%5Bbots%5D=both&common_wiki_other=&sortorder=ascending&output_limit=&doit=Do%20it!&active_tab=tab_output&language=se&page_image=any&regexp_filter=&subpage_filter=either&wpiu=any&sitelinks_no=&manual_list_wiki=&templates_yes=&common_wiki=auto&wikidata_label_language=&output_compatability=catscan&templates_any=&outlinks_yes=&wikidata_prop_item_use=&labels_no=&after=&sortby=size&ores_type=any&links_to_any=&project=wikipedia&min_redlink_count=1&referrer_name=&format=json&smaller=2000&langs_labels_no=&add_image=on&show_redirects=both&source_combination=&sitelinks_yes=&wikidata_item=no&maxlinks=&negcats=&larger=&edits%5Bflagged%5D=both&pagepile=&langs_labels_any=&ores_prob_from=&langs_labels_yes=&interface_language=en&links_to_all=&min_sitelink_count=&combination=subset&ores_prediction=any&links_to_no="

side = """{{DISPLAYTITLE:<span style="font-size: 0px;">#%p</span>}}
{{TNT|Samisk oppgavenavigasjon}}

<div style="display:flex; flex-wrap: wrap">
<div align="justify">
<div style="font-family: trebuchet ms; font-size: 18px>
<div style="margin:20px; margin-top:5px; margin-right:20px">

== '''#%s''' ==
<br/>
<gallery mode="packed" heights="200px" style="color:#000">\n"""

side2 = """{{DISPLAYTITLE:<span style="font-size: 0px;">#%p</span>}}
{{TNT|Samisk oppgavenavigasjon}}

<div style="display:flex; flex-wrap: wrap">
<div align="justify">
<div style="font-family: trebuchet ms; font-size: 18px>
<div style="margin:20px; margin-top:5px; margin-right:20px">

== '''#%s''' ==
<br/>
==='''KORTE SIDER'''===
----
<br/>
<gallery mode="packed" heights="200px" style="color:#000">
#%q1</gallery>
==='''NOE LENGER SIDER'''===
----
<br/>
<gallery mode="packed" heights="200px" style="color:#000">
#%q2</gallery>

[[Fil:Font Awesome 5 solid arrow-alt-circle-up.svg|35px|right|link=#top]]"""

allquery = {
    "queryPersonKvinner":{"title": "KVINNER", "path": "Prosjekt:Samisk kunnskap på nett/Undervisning/For studenter/Oppgave 1/Kvinner","url": "?item wdt:P31 wd:Q5 ; wdt:P21 wd:Q6581072 ;", "limit": "100"},
    "queryPersonMusiker":{"title": "MUSIKERE", "path": "Prosjekt:Samisk kunnskap på nett/Undervisning/For studenter/Oppgave 1/Musikere", "url": "?item wdt:P31 wd:Q5; wdt:P106 wd:Q639669 ;", "limit": "100" },
    "queryPersonKunstnere":{"title": "KUNSTNERE", "path": "Prosjekt:Samisk kunnskap på nett/Undervisning/For studenter/Oppgave 1/Kunstnere", "url": "?item wdt:P31 wd:Q5 ; wdt:P106 wd:Q483501 ;", "limit": "100"},
    "queryPersonForfattere":{"title": "FORFATTERE", "path": "Prosjekt:Samisk kunnskap på nett/Undervisning/For studenter/Oppgave 1/Forfattere", "url": "?item wdt:P31 wd:Q5 ; wdt:P106 wd:Q36180 ;", "limit": "100"},
    "queryStederiTromsogFinnmark":{"title": "STEDER I TROMS OG FINNMARK", "path": "Prosjekt:Samisk kunnskap på nett/Undervisning/For studenter/Oppgave 1/Steder i Troms og Finnmark", "url": "?id wdt:P131* wd:Q52600648 ;", "mapframe": """<mapframe frameless width=1500 height=900 zoom=6 align="center" latitude=70.0718 longitude=23.2172>"""},
    "queryStederisvenskLappland":{"title": "STEDER I SVENSK LAPPLAND", "path": "Prosjekt:Samisk kunnskap på nett/Undervisning/For studenter/Oppgave 1/Steder i svensk Lappland", "url": "?id wdt:P131* wd:Q212640 ;", "mapframe": """<mapframe frameless width=1500 height=900 zoom=6 align="center" latitude=65.929 longitude=16.743>"""},
    "queryStederifinskLappland":{"title": "STEDER I FINSK LAPPLAND", "path": "Prosjekt:Samisk kunnskap på nett/Undervisning/For studenter/Oppgave 1/Steder i finsk Lappland", "url": "?id wdt:P131* wd:Q5700 ;", "mapframe": """<mapframe frameless width=1500 height=900 zoom=6 align="center" latitude=67.9222 longitude=26.5046>"""},
    "queryStubbMusiker":{"title": "MUSIHKKÁRAT (MUSIKERE)", "path": "Prosjekt:Samisk kunnskap på nett/Undervisning/For studenter/Oppgave 2/Musikere", "url": "Musihkk%C3%A1rat%7C5" },
    "queryStubbKunstnere":{"title": "DÁIDDÁRAT (KUNSTNERE)", "path": "Prosjekt:Samisk kunnskap på nett/Undervisning/For studenter/Oppgave 2/Kunstnere", "url": "D%C3%A1idd%C3%A1rat%7C5" },
    "queryStubbForfattere":{"title": "GIRJEČÁLLIT (FORFATTERE)", "path": "Prosjekt:Samisk kunnskap på nett/Undervisning/For studenter/Oppgave 2/Forfattere", "url": "Girje%C4%8D%C3%A1llit%7C5" },
    "queryStubbPolitikere":{"title": "SÁPMELAŠ POLITIHKKÁRAT (SAMISKE POLITIKERE)", "path": "Prosjekt:Samisk kunnskap på nett/Undervisning/For studenter/Oppgave 2/Samiske politikere", "url": "S%C3%A1pmela%C5%A1%20politihkk%C3%A1rat%7C5" },
}
arrow = "\n\n[[Fil:Font Awesome 5 solid arrow-alt-circle-up.svg|35px|right|link=#top]]"

bottom = "</gallery>" + arrow
bottom2 = "\n</mapframe>" + arrow
